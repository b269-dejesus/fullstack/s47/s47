// "document" refers to the whole webpage
//querySelector() is used to select a specific object (Html element) from our document(webpage)
const	txtFirstName = document.querySelector("#txt-first-name");
const	txtLastName = document.querySelector("#txt-last-name");

const	spanFullName = document.querySelector("#span-full-name");


// document.getElementByID
// document.getElementByClassname

// const	txtFirstName = document.getElementByID("#txt-first-name");

// const	spanFullName = document.GetElementByID("#span-full-name");


/*txtFirstName.addEventListener('keyup', (event) =>{
	spanFullName.innerHTML = txtFirstName.value;
	console.log(event.target);
	console.log(event.target.value);
});
*/
// Actvity S46
function combineFullName(){

	let textFirstName  = txtFirstName.value;
	let textLastName = txtLastName.value;
	spanFullName.innerHTML = `${textFirstName} ${textLastName}`;
};
txtFirstName.addEventListener("keyup", combineFullName);
txtLastName.addEventListener("keyup", combineFullName);


//  =>{
// 	spanFullName.innerHTML = txtFirstName.value;
// 	console.log(event.target);
// 	console.log(event.target.value);
// });

